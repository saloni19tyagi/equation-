import sys


def reversestring(givenstring):
	newstring = ""
	prevop = len(givenstring)
	for op in range(-1,len(givenstring)*-1 -1,  -1):
	
		if givenstring[op] in ['+' , '-']:
			newstring += givenstring[op : prevop]
			prevop = op
	return newstring

givenstring = sys.argv[1]
if givenstring[0] != '-':
	givenstring = '+' + givenstring
print(reversestring(givenstring))